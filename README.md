# sas2irid

SAS2 IR Identify Device

## Summary

This utility enumerates all LSI SAS controller-attached disks along with each device's GUID and kernel device node (eg. /dev/sda).

Its useful when your disk devices are connected to a dumb SAS backplane (without SAS expander function) through a LSI HBA flashed with the IT (non-raid, passthrough) firmware such as the LSI 9207-8i. In such cases, kernel enclosure services at `/sys/class/enclosure` are unavailable and it becomes difficult to correlate disk physical to logical locations, increasing the risk of error during sensitive operations such as swapping out a failed disk.

It's inspired from a Perl script [posted](http://en.community.dell.com/techcenter/b/techcenter/archive/2013/08/07/determining-drive-slot-location-on-linux-for-lsi-9207-sas-controllers) years ago to a Dell technical forum.

## Prerequisites

To use `sas2irid`, you need LSI's `sas2ircu` utility which can be obtained from the Broadcom [website](https://docs.broadcom.com/docs/SAS2IRCU_P20.zip) or the [hwraid.le-vert.net package repository](http://hwraid.le-vert.net/wiki/DebianPackages)

The extra `tabulate` Python module is required. On a Debian-based systems, it may be installed with the following command:

```
sudo apt install python3-tabulate
```

## Usage

```
usage: sas2irid [-h] [-d DEVICE] [-lon] [-loff] [-n] [-p]

Correlate SAS enclosure slots to kernel device names

optional arguments:
  -h, --help            show this help message and exit
  -d DEVICE, --device DEVICE
                        Kernel device path (eg. /dev/sda)
  -lon, --locate-on     Turn on LOCATE LED for specified device
  -loff, --locate-off   Turn off LOCATE LED for specified device
  -n, --noheader        Omit headers from output
  -p, --plain           Output table in plain format
```
